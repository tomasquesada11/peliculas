import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { Dimensions, ScrollView, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Image, StyleSheet, Text, View } from 'react-native'
import { RootStackParams } from '../navigation/Navigation';
import { useMovieDetails } from '../hooks/useMovieDetails';
import { MovieDetails } from '../components/MovieDetails';
import Icon from 'react-native-vector-icons/Ionicons';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const screenHeight = Dimensions.get('screen').height;

interface Props extends StackScreenProps<RootStackParams, 'DetailScreen'> {};

export const DetailScreen = ( { route, navigation } : Props ) => {

    const { top } = useSafeAreaInsets();

    const movie = route.params;
    const uri = `https://image.tmdb.org/t/p/w500${ movie.poster_path }`;

    const { isLoading, movieFull, cast } = useMovieDetails( movie.id );
    
    return (

        <ScrollView>

            <View style={ styles.imageContainer } >

                <View style={ styles.imageBorder } >

                    <Image
                        style={ styles.posterImage }
                        source={{ uri }}
                    />

                </View>
                
            </View>

            <View style={ styles.marginContainer } >

                <Text style={ styles.subtitle } >{ movie.original_title }</Text>

                <Text style={ styles.title } >{ movie.title }</Text>

            </View>

            {
                isLoading
                ? <ActivityIndicator size={ 35 } color='grey' style={{ marginTop: 20 }} />
                : <MovieDetails movieFull={ movieFull! } cast={ cast } />
            }

            {/* Boton para cerrar */}

            <TouchableOpacity
                style={{ ...styles.backButton, top: top + 10 }}
                onPress={ () => navigation.pop() }
            >

                <Icon 
                    color='white'
                    name='chevron-back-outline'
                    size={ 60 }
                />

            </TouchableOpacity>

        </ScrollView>
        
    )
}

const styles = StyleSheet.create({
    imageContainer: {
        width: '100%',
        height: screenHeight * 0.7,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.24,
        shadowRadius:7,

        elevation: 9,
        borderBottomStartRadius: 25,
        borderBottomEndRadius: 25,
    },
    imageBorder: {
        flex: 1,
        overflow: 'hidden',
        borderBottomStartRadius: 25,
        borderBottomEndRadius: 25,
    },
    posterImage: {
        flex: 1
    },
    marginContainer: {
        marginHorizontal: 20,
        marginTop: 20
    },
    subtitle: {
        fontSize: 16,
        opacity: 0.7,
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    backButton: {
        position: 'absolute',
        zIndex: 999,
        elevation: 9,
    }
});
