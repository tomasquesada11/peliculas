import axios from "axios";

const movieDB = axios.create({
    baseURL: 'https://api.themoviedb.org/3/movie',
    params: {
        api_key: 'f8ee156d486b2191e2d75b8d60a1f7be',
        language: 'es-ES'
    }
});

export default movieDB;